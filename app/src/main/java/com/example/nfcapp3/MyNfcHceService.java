package com.example.nfcapp3;

import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

/**
 * Created by ttlnisoffice on 1/22/18.
 */

public class MyNfcHceService extends HostApduService {
    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle bundle) {
        Log.d("Apdu", "processCommandApdu");
        android.os.Debug.waitForDebugger();
        String t = "";
        //Toast.makeText(this, "ttt", Toast.LENGTH_SHORT).show();
        //byte[] message = new byte[1];
        //message[0] = (byte) 0xD4387FB9;
        return "D4387FB9".getBytes();
    }

    private String byteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    @Override
    public void onDeactivated(int i) {
        Log.d("Apdu", "Deactivated");
    }
}
